package com.designPatterns.strategy;

public class designPatternsMain {

	public static void main(String[] args) {
		StrategyContex context = new StrategyContex(new OperationAdd());		
	    System.out.println("10 + 5 = " + context.executeStrategy(10, 5));

	    context = new StrategyContex(new OperationSubstract());		
	    System.out.println("10 - 5 = " + context.executeStrategy(10, 5));

	     context = new StrategyContex(new OperationMultiply());		
	     System.out.println("10 * 5 = " + context.executeStrategy(10, 5));

	}

	
	
}
