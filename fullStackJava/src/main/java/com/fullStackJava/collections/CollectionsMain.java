package com.fullStackJava.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

public class CollectionsMain {

	
	public static void main(String[] args){
		CollectionsMain app = new CollectionsMain();
	
		int[] intVariableArray = {1,2,3,4,5,6,7,8,9,10};
	    
	    ArrayList<String> stringArrayList = new ArrayList<>();
	    Vector<String> stringList = new Vector<>();
	    
	    List<Object> linkedListString = new LinkedList<>();
	    linkedListString.add("Julian");
	    linkedListString.add("Roberto");
	    linkedListString.add("Manuel");
	    linkedListString.add("Armando");
	    
	    System.out.println("***** Original List ***");
	    app.printList_ForEach(linkedListString);
	    
	    linkedListString.sort(null);
	    System.out.println("***** linkedListString.sort Null/default ordered List ***");
	    app.printList_ForEach(linkedListString);
	    
	    Collections.reverse(linkedListString);
	    System.out.println("***** Collections.reverse ordered List ***");
	    System.out.println("***** printList_Iterator ***");
	    app.printList_Iterator(linkedListString);
	    
	    List<Object> personList = new ArrayList<>();
	    personList.add(new Person("Julio"));
	    personList.add(new Person("Roberto"));
	    personList.add(new Person("ManuelAlberto"));
	    personList.add(new Person("Armandito"));
	    
	    personList.sort(null);
	    System.out.println("***** personList.sort Null/default personList ***");
	    app.printList_ForEach(personList);
	    
	    Collections.reverse(personList);
	    System.out.println("***** Collections.reverse Null/default personList ***");
	    app.printList_ForEach(personList);
	    
	
		
	    Stack<String> stackStrig = new Stack<>();

	}
	
	
	public void printList_ForEach(List<java.lang.Object> list) {
		for (Object element : list) {
			System.out.println("Element: " + element);
		}
	}
	
	public void printList_Iterator(List<Object> list) {
		Iterator<Object> iterator = list.iterator();
		while(iterator.hasNext()) {
			System.out.println("Element: " + iterator.next());
		}
	}
}
