package com.fullStackJava.collections;

public class Person  implements Comparable<Person> {

	private String name;
	
	
	public Person(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Person obj) {
		if( this.name.length() == obj.getName().length()) {
			return 0;
		}
		else if( this.name.length() >  obj.getName().length() ) {
			return 1;
		}
		else {
			return -1;
		}
	}

	@Override
	public String toString() {
		return "Person [name=" + name + "]";
	}

}
