package com.fullStackJava.section3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.Vector;

import com.fullStackJava.staticTest.StaticTest;
import com.google.common.primitives.UnsignedInteger;
import com.google.common.primitives.UnsignedInts;
import com.google.common.primitives.UnsignedLongs;

public class PrimitiveDataTypes {

    public static void main(String[] args){
    	
//        System.out.println("int min  32 bits= " + (-2147483648));
//        System.out.println("int MAX  32 bits= " + 2147483647);
//        System.out.println("shorMin  = " + (-32768));
//        System.out.println("shortMAX = " + 32767);

//        System.out.println("longMAX  = " + 9223372036854775807L);
//        System.out.println("byteMin  = " + (-128));
//        System.out.println("byteMAX  = " + 127);
        
    	System.out.println("*** INTEGERS ***");
        displaySizeMinAndMax(Byte.TYPE, Byte.SIZE, Byte.MIN_VALUE, Byte.MAX_VALUE);
		displaySizeMinAndMax(Short.TYPE, Short.SIZE, Short.MIN_VALUE, Short.MAX_VALUE);
		displaySizeMinAndMax(Character.TYPE, Character.SIZE, (int) Character.MIN_VALUE, (int) Character.MAX_VALUE);
		displaySizeMinAndMax(Integer.TYPE, Integer.SIZE, Integer.MIN_VALUE, Integer.MAX_VALUE);
		displaySizeMinAndMax(Long.TYPE, Long.SIZE, Long.MIN_VALUE, Long.MAX_VALUE);
		
    	System.out.println("*** DECIMALS ***");
    	displaySizeMinAndMax(Float.TYPE, Float.SIZE, Float.MIN_VALUE, Float.MAX_VALUE);
		displaySizeMinAndMax(Double.TYPE, Double.SIZE, Double.MIN_VALUE, Double.MAX_VALUE);
		
		System.out.println("*** CHAR ***");
    	displaySizeMinAndMax(Character.TYPE, Character.SIZE, new Integer(Character.MIN_VALUE), new Integer(Character.MAX_VALUE));
    	
    	System.out.println("*** BOOLEAN ***");
    	System.out.printf("type:%-6s size:%-2s    :%-20s    :%s\n", Boolean.TYPE, 2, Boolean.TRUE, Boolean.FALSE);
		
    	System.out.println("*** BIGDECIMAL ***");
    	System.out.printf("type:%-6s       :%s\n", BigDecimal.class.getTypeName(), "Immutable, arbitrary-precision integer unscaled value and a 32-bit integer scale.");
    	
    	int decimal = 100;
    	int octal = 0144;
    	int hexa = 0x64;
    	int binary = 0b00101;
    	byte byteVariable =-128;
    	
    	System.out.println("binary: " + decimal);
    	System.out.println("octal: " + octal);
    	System.out.println("hexa: " + hexa);
    	System.out.println("binary: " + binary);
    	System.out.println("byteVariable: " + byteVariable);
    	
    	float f = 1.1f;
    	double d = 2.2d;
    	System.out.println("float: " +f);
    	System.out.println("double: " +d); 
    	
    	printUnSignedExamples();
    	
    	printStaticVariable();
    	
    	 Scanner scanner = new Scanner(System.in);
         System.out.println("Enter your name: ");
         String input = scanner.next();
         System.out.println("Scanned: bye bye " + input );
         
    }
    
    public String phone() {
    	String result="";
    	System.out.println(result);
		return result;
    }
    
    public static void displaySizeMinAndMax(Class<?> type, int size, Number min, Number max) {
		System.out.printf("type:%-6s size:%-2s min:%-20s max:%s\n", type, size, min, max);
	}
    
    public static void printUnSignedExamples() {
    	System.out.println();
    	
    	int vInt = Integer.parseUnsignedInt("4294967295");
        System.out.println("parseUnsignedInt: " + vInt); // -1
        String sInt = Integer.toUnsignedString(vInt);
        System.out.println("toUnsignedString:" + sInt); // 4294967295

        long vLong = Long.parseUnsignedLong("18446744073709551615");
        System.out.println("parseUnsignedLong: " +vLong); // -1
        String sLong = Long.toUnsignedString(vLong);
        System.out.println("toUnsignedString: " +sLong); // 18446744073709551615

        // Guava 18.0
        int vIntGu = UnsignedInts.parseUnsignedInt(UnsignedInteger.MAX_VALUE.toString());
        System.out.println("parseUnsignedInt: " + vIntGu); // -1
        String sIntGu = UnsignedInts.toString(vIntGu);
        System.out.println("UnsignedInts: " +sIntGu); // 4294967295

        long vLongGu = UnsignedLongs.parseUnsignedLong("18446744073709551615");
        System.out.println("parseUnsignedLong: " + vLongGu); // -1
        String sLongGu = UnsignedLongs.toString(vLongGu);
        System.out.println("UnsignedLongs: " +sLongGu); 
        
        System.out.println();
        
       
        
    }
    
    public static void printStaticVariable() {
    	 System.out.println();
    	 
    	 //StaticTest staticTestObj = new StaticTest();
    	 System.out.println("StaticTest.staticVaraiable value = " + StaticTest.getStaticVaraiable());
    	 StaticTest.setStaticVaraiable(38);
    	 System.out.println("StaticTest.staticVaraiable value = " + StaticTest.getStaticVaraiable());
    	 
    	 //StaticTest staticTestObj2 = new StaticTest();
    	 System.out.println("StaticTest.staticVaraiable value = " + StaticTest.getStaticVaraiable());
    	 
    	 System.out.println();
    
    	
    	 
    	 
    }
    
}
